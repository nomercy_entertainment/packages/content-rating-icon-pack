// import * as i from './index.mjs';

import { fileURLToPath } from 'url';
import fs from 'fs';
import path from 'path';
import sharp from 'sharp';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export default async () => {
	const languages = fs.readdirSync(`${__dirname}/src`);
	
	fs.rmSync(`${__dirname}/dist`, { recursive: true });
	fs.rmSync(`${__dirname}/temp`, { recursive: true });

	for (let i = 0; i < languages.length; i++) {
		const files = fs.readdirSync(`${__dirname}/src/${languages[i]}`).filter((f) => f.endsWith('.svg'));
		const langFile = [];

		const svgFolder = `${__dirname}/dist/svg/${languages[i]}`;
		const pngFolder = `${__dirname}/dist/png/${languages[i]}`;
		const tempFolder = `${__dirname}/temp/${languages[i]}`;
		
		fs.mkdirSync(svgFolder, { recursive: true });
		fs.mkdirSync(pngFolder, { recursive: true });
		fs.mkdirSync(tempFolder, { recursive: true });
		
		for(let j = 0; j < files.length; j++) {
			const newFilename = files[j].replace('.svg', '');
			
			let content = fs.readFileSync(`${__dirname}/src/${languages[i]}/${files[j]}`, { encoding: 'utf8' });
			content = content.replace(/<title>.*<\/title>/, '');
			content = content.replace(/style="fill:#[\w\d]+"/g, '');

			content = content.replace(/<path d="([\w\d";'_\.-=,\s]+)style="fill:none;stroke:#([\w\d]+)"\/>/, `<path id="${newFilename}" d="$1style="fill:none;stroke:currentColor"/>`);
			content = content.replace(/<path d="/g, `<path id="${newFilename}" fill="currentColor" d="`);
			content = content.replace(/<polygon points="/g, `<polygon id="${newFilename}" fill="currentColor" points="`);

			fs.writeFileSync(`${svgFolder}/${files[j]}`, content, { overwrite: true });

			await createPngImages(content, tempFolder, pngFolder, newFilename, langFile);
		};

		fs.rmSync(tempFolder, { recursive: true });

	};
};

const createPngImages = async (content, tempFolder, pngFolder, filename, langFile, sizes = [48, 92, 128, 265, 512], colors = ['white', 'black']) => {

	for (let i = 0; i < colors.length; i++) {
		let newContent = content.replace(/currentColor/g, colors[i]);
		newContent = newContent.replace('viewBox="0 0 24 24"', 'viewBox="0 0 24 24" width="512" height="512"');

		let tempFile = `${tempFolder}/${filename}-${colors[i]}.svg`;
		
		fs.writeFileSync(tempFile, newContent, { overwrite: true });

		for (let j = 0; j < sizes.length; j++) {
			await new Promise((resolve, reject) => {
				sharp(tempFile)
					.png({
						compressionLevel: 9,
					})
					.resize(sizes[j])
					.toFile(`${pngFolder}/${filename}-${sizes[j]}px-${colors[i]}.png`)
					.then(() => {
						resolve();
					});
			})
		}
	}
};
